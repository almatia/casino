FROM python:latest

COPY casino.py /casino.py

CMD ["python", "/casino.py"]
